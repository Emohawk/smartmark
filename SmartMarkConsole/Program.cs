﻿using System;
using System.Linq;
using ImgProc = ImageProcessor;

namespace SmartMarkConsole
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var file = !args.Any() ? @"D:\Stuff\Images\TestPic.jpg" : args[0];

            var ip = new ImgProc.ImageProcessor();

            ip.MarkImage(file, "Tikeb 2017");

            Console.WriteLine("Done");
            Console.ReadKey();
        }
    }
}