﻿using System;
using System.Collections.Generic;
using System.Drawing;
using ImageProcessor.Helpers;
using Emgu.CV;
using Emgu.CV.Structure;

namespace ImageProcessor
{
    public class ImageProcessor
    {
        private readonly ImageHelper _imgHelper = new ImageHelper();

        public void MarkImage(string file, string text)
        {
            // Convert the image to grey scale using the specified colour and convert to a bitmap
            var bwImg = ProcessFunction(file, "#0C0B07"); //#51151D #926745 #6C9084 #0C0B07
            // Get the image details
            var fileInfo = _imgHelper.GetImageDetails(file);

            var data = bwImg.Data;

            var ls = new List<int[]>();

            for (var r = bwImg.Rows - 1; r >=0; r--)
            {
                for (var c = bwImg.Cols - 1; c >= 0; c--)
                {
                    var col = data[r, c, 0];

                    if (col == 255)
                    {
                        ls.Add(new []{c, r});
                    }
                }
            }

            var cc = ls[ls.Count / 2];

            // Original image
            var bmp = new Bitmap(file);
            var gra = Graphics.FromImage(bmp);

            gra.DrawString(text, new Font("Verdana", 24), Brushes.White, new PointF(cc[0], cc[1]));

            bmp.Save(@"D:\Stuff\Images\TestPic_Marked.jpg");
            bwImg.Bitmap.Save(@"D:\Stuff\Images\TestPic_GrayScale.jpg");
        }

        private Image<Gray, byte> ProcessFunction(string file, string colourHex)
        {
            var colour = ColorTranslator.FromHtml(colourHex);

            var lowerR = colour.R * 0.5;
            var lowerG = colour.G * 0.5;
            var lowerB = colour.B * 0.5;

            var upperR = colour.R * 1.5;
            var upperG = colour.G * 1.5;
            var upperB = colour.B * 1.5;

            var imgOrg = new Image<Bgr, byte>(file);

            var imgProc = imgOrg.InRange(new Bgr(lowerB, lowerG, lowerR), new Bgr(upperB, upperG, upperR));
            return imgProc;
        }
    }
}