﻿namespace ImageProcessor.Models
{
    public class ImageModel
    {
        public string FileName { get; set; }
        public string FileExtension { get; set; }
        public string FilePath { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public long SizeInBytes { get; set; }

        public string FullPath => $"{FilePath}\\{FileName}";
    }
}