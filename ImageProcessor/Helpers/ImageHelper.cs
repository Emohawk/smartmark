﻿using System.Drawing;
using System.IO;
using ImageProcessor.Models;

namespace ImageProcessor.Helpers
{
    public class ImageHelper
    {
        public ImageModel GetImageDetails(string file)
        {
            var img = Image.FromFile(file);
            var fileInfo = new FileInfo(file);

            var imageModel = new ImageModel
            {
                FileName = fileInfo.Name,
                FilePath = fileInfo.DirectoryName,
                FileExtension = fileInfo.Extension,
                Width = img.Width,
                Height = img.Height,
                SizeInBytes = fileInfo.Length
            };

            return imageModel;
        }
    }
}